package application;

import java.util.ArrayList;
import java.util.List;
 
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
 
public class TestJeu extends Application {

	Label label1 = new Label("X");
	
	private boolean playable = true;
	private boolean turnX = true;
	private Tile[][] board = new Tile[5][5];
	private List<Combo> combos = new ArrayList<>();
	 
	private Pane root = new Pane();
	 
	private Parent createContent() {
	root.setPrefSize(1200, 1200);
	 
	Button button = new Button("Retour");
	button.setLayoutX(1050);
	button.setLayoutY(200);
	
	Button button1 = new Button("Enregistrer");
	button1.setLayoutX(1010);
	button1.setLayoutY(250);

	Button button2 = new Button("Charger");
	button2.setLayoutX(1100);
	button2.setLayoutY(250);

	Button button3 = new Button("Annuler dernier coup");
	button3.setLayoutX(1050);
	button3.setLayoutY(300);
	
	Label label0 = new Label("Alignez 3 pions pour gagner");
	label0.setLayoutX(1030);
	label0.setLayoutY(50);

	label1.setLayoutX(1020);
	label1.setLayoutY(100);
	label1.setFont(Font.font(36));
	Label label2 = new Label("Choisissez votre case");
	label2.setLayoutX(1050);
	label2.setLayoutY(120);
	
	button.setOnAction(new EventHandler<ActionEvent>() {
	
	    @Override
	    public void handle(ActionEvent event) {
	    	if (label1.getText() == "O") {
	           label1.setText("X");
	    	}
	    	else {
	            label1.setText("O");
	     	}
	    }
	});
	
	root.getChildren().addAll(button,button1,button2,button3, label0, label1,label2);
	
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			Tile tile = new Tile();
			tile.setTranslateX(j * 200);
			tile.setTranslateY(i * 200);
			root.getChildren().add(tile);
			board[j][i] = tile;
		}
	}
	 
	// horizontal
	for (int y = 0; y < 5; y++) {
		combos.add(new Combo(board[0][y], board[1][y], board[2][y]));
	}
	 
	// vertical
	for (int x = 0; x < 5; x++) {
		combos.add(new Combo(board[x][0], board[x][1], board[x][2]));
	}
	 
	// diagonals
	combos.add(new Combo(board[0][0], board[1][1], board[2][2]));
	combos.add(new Combo(board[2][0], board[1][1], board[0][2]));
	 
	return root;
	}
	 
	@Override
	public void start(Stage primaryStage) throws Exception {
	primaryStage.setScene(new Scene(createContent()));
	primaryStage.show();
	}
	 
	private void checkState() {
		for (Combo combo : combos) {
			if (combo.isComplete()) {
				playable = false;
				playWinAnimation(combo);
				break;
			}
		}
	}
	 
	private void playWinAnimation(Combo combo) {
		Line line = new Line();
		line.setStartX(combo.tiles[0].getCenterX());
		line.setStartY(combo.tiles[0].getCenterY());
		line.setEndX(combo.tiles[0].getCenterX());
		line.setEndY(combo.tiles[0].getCenterY());
		 
		root.getChildren().add(line);
		 
		Timeline timeline = new Timeline();
		timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
		new KeyValue(line.endXProperty(), combo.tiles[2].getCenterX()),
		new KeyValue(line.endYProperty(), combo.tiles[2].getCenterY())));
		timeline.play();
	}
	 
	private class Combo {
		private Tile[] tiles;
		public Combo(Tile... tiles) {
			this.tiles = tiles;
		}
		 
		public boolean isComplete() {
			if (tiles[0].getValue().isEmpty()) return false;		 
		return tiles[0].getValue().equals(tiles[1].getValue()) &&
		       tiles[0].getValue().equals(tiles[2].getValue());
		}
	}
	 
	private class Tile extends StackPane {
		private Text text = new Text();
		 
		public Tile() {
			Rectangle border = new Rectangle(200, 200);
			border.setFill(null);
			border.setStroke(Color.BLACK);
			 
			text.setFont(Font.font(72));
			 
			setAlignment(Pos.CENTER);
			getChildren().addAll(border, text);
			 
			setOnMouseClicked(event -> {
				if (!playable) {
					return;
				}
				 
				if (event.getButton() == MouseButton.PRIMARY) {
					if (!turnX) return;
						drawX();
						turnX = false;
						label1.setText("O");
						checkState();
					}
					else if (event.getButton() == MouseButton.SECONDARY) {
						if (turnX) return;
						drawO();
						turnX = true;
						label1.setText("X");
						checkState();
						}
				}
			);
		}
		 
		public double getCenterX() {
			return getTranslateX() + 100;
		}
		 
		public double getCenterY() {
			return getTranslateY() + 100;
		}
		 
		public String getValue() {
			return text.getText();
		}
		 
		private void drawX() {
			text.setText("X");
		}
		 
		private void drawO() {
			text.setText("O");
		}
	}
	 
	public static void main(String[] args) {
		launch(args);
	}
}
 