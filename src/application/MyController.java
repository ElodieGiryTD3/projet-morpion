package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
 
public class MyController implements Initializable {
 
   @FXML
   private Button myButton;
   @FXML
   private TextField myTextField;
   @FXML
   private ProgressBar progressBar;
   @FXML
   private ProgressIndicator progressIndicator;
   
   int iFinal;
  
   @Override
   public void initialize(URL location, ResourceBundle resources) {
 
       // TODO (don't really need to do anything here).
      
   }
   
   @FXML
   public void startProgress1(ActionEvent event) {
	   Task<Void> task = new Task<Void>() {
		   @Override
		   protected Void call() throws Exception {
			   for(int i=0; i<=100; i++) {
				   updateProgress(i, 100);
				   updateMessage("bonjour "+i);
				   Thread.sleep(100);
			   }
			   return null;
		   }
	   };
	   
	   task.messageProperty().addListener(new ChangeListener<String>() {
		   @Override
		   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			   myTextField.setText(newValue + " Reading ...");
		   }
	   });
	   
	   progressBar.progressProperty().unbind();
	   progressBar.progressProperty().bind(task.progressProperty());
	   
	   Thread th = new Thread(task);
	   th.setDaemon(true);
	   th.start();
   }

   
	public static HashMap<Integer, Coup> loadCoupsFromFile(String file){
		System.out.println("loadCoupsFromFile from "+file+" ...");
		HashMap<Integer, Coup> map = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));
			String s = "";
			while ((s = br.readLine()) != null) {
				String[] sIn = s.split("\t")[0].split(" ");
				String[] sOut = s.split("\t")[1].split(" ");

				double[] in = new double[sIn.length];
				double[] out = new double[sOut.length];

				for (int i = 0; i < sIn.length; i++) {
					in[i] = new Double(sIn[i]);
				}

				for (int i = 0; i < sOut.length; i++) {
					out[i] = new Double(sOut[i]);
				}

				Coup c = new Coup(9, "");
				c.in = in ;
				c.out = out ;
				map.put(map.size(), c);
			}
			br.close();
		} 
		catch (Exception e) {
			System.out.println("Test.loadCoupsFromFile()");
			e.printStackTrace();
			System.exit(-1);
		}
		return map ;
	}
	
   public void test(ActionEvent event){
		try {
			System.out.println("Start learning ...");
			
			Task<Void> task = new Task<Void>() {

			    int size = 9;
				int[] layers = new int[]{ size, 128, size };
				double error = 0.0 ;
				MultiLayerPerceptron net = new MultiLayerPerceptron(layers, 0.1, new SigmoidalTransferFunction());
				double epochs = 1000000 ;

				HashMap<Integer, Coup> mapTrain = loadCoupsFromFile("./resources/train_dev_test/train.txt");
				HashMap<Integer, Coup> mapDev = loadCoupsFromFile("./resources/train_dev_test/dev.txt");
				HashMap<Integer, Coup> mapTest = loadCoupsFromFile("./resources/train_dev_test/test.txt");
				//TRAINING ...
   
			   @Override
			   protected Void call() throws Exception {
				   
				   for(int i = 0; i < epochs; i++){
					   Coup c = null ;
						while ( c == null )
							c = mapTrain.get((int)(Math.round(Math.random() * mapTrain.size())));
							error += net.backPropagate(c.in, c.out);
						if ( i % 10000 == 0 ) {
							updateProgress(i, epochs);
							updateMessage("Error at step "+i+" is "+ (error/(double)i));
							//Thread.sleep(100);
						}
				   }
				   
					error /= epochs ;
					if ( epochs > 0 )
						updateProgress(epochs, epochs);
						updateMessage("Error is "+error);
					System.out.println("Learning completed!");
					
					//TEST ...
					double[] inputs = new double[]{0.0, 1.0};
					double[] output = net.forwardPropagation(inputs);

					System.out.println(inputs[0]+" or "+inputs[1]+" = "+Math.round(output[0])+" ("+output[0]+")");

				   return null;
			   }
		   };
		   
		   task.messageProperty().addListener(new ChangeListener<String>() {
			   @Override
			   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				   myTextField.setText(newValue);
			   }
		   });
		   
		   progressBar.progressProperty().unbind();
		   progressBar.progressProperty().bind(task.progressProperty());
		   progressIndicator.progressProperty().bind(task.progressProperty());
		   
		   Thread th = new Thread(task);
		   th.setDaemon(true);
		   th.start();
	  

		} 
		catch (Exception e) {
			System.out.println("Test.test()");
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
