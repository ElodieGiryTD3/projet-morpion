package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Launcher extends Application{

	public void start(Stage primaryStage) throws Exception {
        try {
        final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("accueil.fxml"));
        final AnchorPane root = (AnchorPane) fxmlLoader.load();
        final Scene scene = new Scene(root, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Accueil");
        primaryStage.setScene(scene);
        primaryStage.show();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
	
	public static void main(String[] args) {
	    Application.launch(args);
	}
	
}
